# Toggler Project

The project is divided in two main components:

- [Toggler API](./Toggler/) - REST API application
- [TogglerUI](./TogglerUI/) - web interface application

Developed and tested with macOSX