﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Toggler.IntegrationTests.Json
{
    public static class JsonExtensions
    {
        public static HttpContent ToContent(this JToken json)
        {
            var str = JsonConvert.SerializeObject(json);
            return new StringContent(str, Encoding.UTF8, "application/json");
        }

        public static async Task<JToken> ReadAsJsonAsync(this HttpContent content)
        {
            var str = await content.ReadAsStringAsync();
            return str.Length == 0 ? null : JToken.Parse(str);
        }
    }
}
