﻿using Newtonsoft.Json.Linq;

namespace Toggler.IntegrationTests.Json
{
    public static class JsonBuilder
    {
        public class Property
        {
            private readonly string name;
            private readonly JToken json;

            public Property(string name, JToken json)
            {
                this.name = name;
                this.json = json;
            }

            public JToken Set(JToken value)
            {
                this.json[this.name] = value;
                return json;
            }

            public JToken Remove()
            {
                this.json.SelectToken(this.name).Parent.Remove();
                return json;
            }
        }

        public static Property Select(this JToken json, string property)
        {
            return new Property(property, json);
        }
    }
}
