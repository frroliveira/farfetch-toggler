using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Toggler.IntegrationTests.Examples;
using Toggler.IntegrationTests.Fixtures;
using Xunit;

namespace Toggler.IntegrationTests.Tests
{
    public class ToggleTests : IClassFixture<TogglerFactory>
    {
        private readonly TogglerClient client;

        public ToggleTests(TogglerFactory factory)
        {
            this.client = factory.CreateTogglerClient();
        }

        [Fact]
        public async Task Post_ToggleCreate_Created()
        {
            var request = new ToggleCreateExample().Valid();
            var response = await this.client.CreateToggleAsync(request);

            Assert.Equal(HttpStatusCode.Created, response.Http.StatusCode);

            Assert.NotNull(response.Json["id"]);
            Assert.Equal(request["feature"], response.Json["feature"]);
            Assert.Equal(request["enabled"], response.Json["enabled"]);

            Assert.EndsWith("/api/toggles/" + response.Json["id"], response.Http.Headers.Location.OriginalString);
        }

        [Fact]
        public async Task Post_ToggleCreate_CreatedDefaults()
        {
            var request = new ToggleCreateExample().MissingEnabled();
            var response = await this.client.CreateToggleAsync(request);

            Assert.Equal(false, response.Json["enabled"]);
        }

        [Fact]
        public async Task Post_ToggleCreate_BadRequestMissingFields()
        {
            var request = new ToggleCreateExample().MissingFeature();
            var response = await this.client.CreateToggleAsync(request);

            Assert.Equal(HttpStatusCode.BadRequest, response.Http.StatusCode);

            Assert.NotNull(response.Json["errors"]["Feature"]);
        }

        [Fact]
        public async Task Post_ToggleCreate_ConflictDuplicateFeature()
        {
            var request = new ToggleCreateExample().Valid();
            var first = await this.client.CreateToggleAsync(request);
            var second = await this.client.CreateToggleAsync(request);

            Assert.Equal(HttpStatusCode.Created, first.Http.StatusCode);
            Assert.Equal(HttpStatusCode.Conflict, second.Http.StatusCode);
        }

        [Fact]
        public async Task Get_ToggleList_Ok()
        {
            var response = await this.client.ListTogglesAsync();

            Assert.Equal(HttpStatusCode.OK, response.Http.StatusCode);
            Assert.Equal(JTokenType.Array, response.Json.Type);
        }

        [Fact]
        public async Task Get_ToggleList_OkAfterChanges()
        {
            var first = await this.client.ListTogglesAsync();
            await EnsureCreated();
            var second = await this.client.ListTogglesAsync();

            Assert.Equal(HttpStatusCode.OK, second.Http.StatusCode);
            Assert.True(first.Json.Count() < second.Json.Count());
        }

        [Fact]
        public async Task Get_ToggleList_OkFiltered()
        {
            await EnsureCreated();
            var creation = await EnsureCreated();

            var request = (string) creation.Json["feature"];
            var response = await this.client.GetToggleByFeatureAsync(request);

            Assert.Equal(HttpStatusCode.OK, response.Http.StatusCode);
            Assert.Single(response.Json);
        }

        [Fact]
        public async Task Get_ToggleById_Ok()
        {
            var creation = await EnsureCreated();
            var id = (string) creation.Json["id"];

            var response = await this.client.GetToggleByIdAsync(id);

            Assert.Equal(HttpStatusCode.OK, response.Http.StatusCode);
            Assert.Equal(id, response.Json["id"]);
            Assert.Equal(creation.Json["feature"], response.Json["feature"]);
            Assert.Equal(creation.Json["enabled"], response.Json["enabled"]);
        }

        [Fact]
        public async Task Get_ToggleById_NotFound()
        {
            var response = await this.client.GetToggleByIdAsync(new IdExample().Get());

            Assert.Equal(HttpStatusCode.NotFound, response.Http.StatusCode);
        }

        [Fact]
        public async Task Patch_ToggleUpdate_NoContent()
        {
            var creation = await EnsureCreated();
            var request = new ToggleUpdateExample().Valid();
            var response = await this.client.UpdateToggleAsync((string) creation.Json["id"], request);

            Assert.Equal(HttpStatusCode.NoContent, response.Http.StatusCode);
        }

        [Fact]
        public async Task Patch_ToggleUpdate_NotFound()
        {
            var request = new ToggleUpdateExample().Valid();
            var response = await this.client.UpdateToggleAsync(new IdExample().Get(), request);

            Assert.Equal(HttpStatusCode.NotFound, response.Http.StatusCode);
        }

        [Fact]
        public async Task Delete_Toggle_NoContent()
        {
            var creation = await EnsureCreated();
            var response = await this.client.DeleteToggleAsync((string) creation.Json["id"]);

            Assert.Equal(HttpStatusCode.NoContent, response.Http.StatusCode);
        }

        [Fact]
        public async Task Delete_Toggle_NotFound()
        {
            var response = await this.client.DeleteToggleAsync(new IdExample().Get());

            Assert.Equal(HttpStatusCode.NotFound, response.Http.StatusCode);
        }

        private async Task<TogglerClient.Response> EnsureCreated()
        {
            var request = new ToggleCreateExample().Valid();
            return await this.client.CreateToggleAsync(request);
        }
    }
}
