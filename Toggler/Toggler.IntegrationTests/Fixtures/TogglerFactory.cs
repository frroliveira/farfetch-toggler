﻿using Microsoft.AspNetCore.Mvc.Testing;

namespace Toggler.IntegrationTests.Fixtures
{
    public class TogglerFactory : WebApplicationFactory<Startup>
    {
        public TogglerClient CreateTogglerClient()
        {
            return new TogglerClient(this.CreateClient());
        }
    }
}
