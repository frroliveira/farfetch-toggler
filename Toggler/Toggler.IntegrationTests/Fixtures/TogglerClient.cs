﻿using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Toggler.IntegrationTests.Json;

namespace Toggler.IntegrationTests.Fixtures
{
    public class TogglerClient
    {
        public class Response
        {
            public HttpResponseMessage Http { get; set; }
            public JToken Json { get; set; }
        }

        private readonly HttpClient inner;

        public TogglerClient(HttpClient client)
        {
            this.inner = client;
        }

        public async Task<Response> CreateToggleAsync(JToken json)
        {
            var response = await this.inner.PostAsync("/api/toggles", json.ToContent());
            return await CreateResponse(response);
        }

        public async Task<Response> ListTogglesAsync()
        {
            var response = await this.inner.GetAsync("/api/toggles/");
            return await CreateResponse(response);
        }

        public async Task<Response> GetToggleByFeatureAsync(string feature)
        {
            var response = await this.inner.GetAsync("/api/toggles/?feature=" + feature);
            return await CreateResponse(response);
        }

        public async Task<Response> GetToggleByIdAsync(string id)
        {
            var response = await this.inner.GetAsync("/api/toggles/" + id);
            return await CreateResponse(response);
        }

        public async Task<Response> UpdateToggleAsync(string id, JToken json)
        {
            var response = await this.inner.PatchAsync("/api/toggles/" + id, json.ToContent());
            return await CreateResponse(response);
        }

        public async Task<Response> DeleteToggleAsync(string id)
        {
            var response = await this.inner.DeleteAsync("/api/toggles/" + id);
            return await CreateResponse(response);
        }

        private async Task<Response> CreateResponse(HttpResponseMessage response)
        {
            var content = await response.Content.ReadAsJsonAsync();

            return new Response
            {
                Http = response,
                Json = content
            };
        }
    }
}
