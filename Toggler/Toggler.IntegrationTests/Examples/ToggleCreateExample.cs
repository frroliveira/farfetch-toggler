﻿using System;
using Newtonsoft.Json.Linq;

namespace Toggler.IntegrationTests.Examples
{
    public class ToggleCreateExample
    {
        public JToken Valid()
        {
            return new JObject()
                .Feature().Set("IsButtonEnabled_" + new Random().Next().ToString())
                .Enabled().Set(true);
        }

        public JToken MissingFeature()
        {
            return Valid().Feature().Remove();
        }

        public JToken MissingEnabled()
        {
            return Valid().Enabled().Remove();
        }
    }
}
