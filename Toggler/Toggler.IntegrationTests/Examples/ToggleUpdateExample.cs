﻿using Newtonsoft.Json.Linq;

namespace Toggler.IntegrationTests.Examples
{
    public class ToggleUpdateExample
    {
        public JToken Valid()
        {
            return new JObject().Enabled().Set(false);
        }
    }
}
