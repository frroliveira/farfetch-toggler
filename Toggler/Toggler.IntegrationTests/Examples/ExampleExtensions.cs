﻿using Newtonsoft.Json.Linq;
using Toggler.IntegrationTests.Json;

namespace Toggler.IntegrationTests.Examples
{
    public static class ExampleExtensions
    {
        public static JsonBuilder.Property Feature(this JToken json)
        {
            return json.Select("feature");
        }

        public static JsonBuilder.Property Enabled(this JToken json)
        {
            return json.Select("enabled");
        }
    }
}
