﻿using System;

namespace Toggler.IntegrationTests.Examples
{
    public class IdExample
    {
        public string Get()
        {
            return "id_" + new Random().Next().ToString();
        }
    }
}
