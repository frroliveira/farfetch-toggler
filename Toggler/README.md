# Toggler API

This solution provides a REST API to manage feature flags across several services.

## Prerequisites

* [ASP.Net Core 2.2](https://dotnet.microsoft.com/download/dotnet-core)

## Quick Start

To build the solution run

    dotnet build

To run the unit and integration tests run

    dotnet test

To start the API run

    cd Toggler
    dotnet run

This will start the server at https://localhost:5001.

The swagger documentation will be available at https://localhost:5001/swagger.

## Development

Implementation is built mainly using:

* [.NET Core](https://docs.microsoft.com/en-us/dotnet/welcome)
* [ASP.NET Core](https://docs.microsoft.com/en-gb/aspnet/core/fundamentals/)
* [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/)

Other libraries are:

* [xUnit](https://xunit.net/) for unit and integration tests
* [moq4](https://github.com/moq/moq4) for mocking in unit tests
* [Swashbuckle](https://github.com/domaindrivendev/Swashbuckle.AspNetCore) for generating and render Swagger docs
* [Swashbuckle Filters](https://github.com/mattfrear/Swashbuckle.AspNetCore.Filters) for adding request and response examples

## Roadmap

* Database choice and tests
* Authentication
* Authorization
* Logging
* Metrics
* Application configuration
* End to end tests
* Event publishing
* Docker container configuration