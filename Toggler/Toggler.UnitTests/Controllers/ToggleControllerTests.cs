using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Toggler.Controllers;
using Toggler.Mappers;
using Toggler.Models.Api;
using Toggler.Models.Database;
using Toggler.Repositories;
using Xunit;

namespace Toggler.UnitTests
{
    public class ToggleControllerTests
    {
        private readonly string testId = "test-id";
        private readonly string testFeature = "test-feature";

        [Fact]
        public async Task Create_ReturnsCreated()
        {
            var repository = new Mock<IToggleRepository>();
            var controller = new TogglesController(repository.Object, new ToggleMapper());

            repository
                .Setup(r => r.CreateAsync(It.IsAny<Toggle>()))
                .ReturnsAsync(true);

            var result = await controller.Create(new ToggleCreate());

            Assert.IsAssignableFrom<CreatedAtActionResult>(result.Result);
        }

        [Fact]
        public async Task Create_ReturnsConflict()
        {
            var repository = new Mock<IToggleRepository>();
            var controller = new TogglesController(repository.Object, new ToggleMapper());

            var request = new ToggleCreate { Feature = testFeature };

            repository
                .Setup(r => r.GetByFeatureAsync(request.Feature))
                .ReturnsAsync(new Toggle());

            var result = await controller.Create(request);

            Assert.Equal(409, Assert.IsAssignableFrom<ObjectResult>(result.Result).StatusCode);
        }

        [Fact]
        public async Task List_ReturnsOk()
        {
            var repository = new Mock<IToggleRepository>();
            var controller = new TogglesController(repository.Object, new ToggleMapper());

            repository
                .Setup(r => r.ListAsync())
                .ReturnsAsync(new List<Toggle> { new Toggle() });

            var result = await controller.List("");

            Assert.IsAssignableFrom<OkObjectResult>(result.Result);
        }

        [Fact]
        public async Task List_Filter_ReturnsOk()
        {
            var repository = new Mock<IToggleRepository>();
            var controller = new TogglesController(repository.Object, new ToggleMapper());

            repository
                .Setup(r => r.GetByFeatureAsync(testFeature))
                .ReturnsAsync(new Toggle());

            var result = await controller.List(testFeature);

            Assert.IsAssignableFrom<OkObjectResult>(result.Result);
        }

        [Fact]
        public async Task Get_ReturnsOk()
        {
            var repository = new Mock<IToggleRepository>();
            var controller = new TogglesController(repository.Object, new ToggleMapper());

            repository
                .Setup(r => r.GetByIdAsync(testId))
                .ReturnsAsync(new Toggle());

            var result = await controller.Get(testId);

            Assert.IsAssignableFrom<OkObjectResult>(result.Result);
        }

        [Fact]
        public async Task Get_ReturnsNotFound()
        {
            var repository = new Mock<IToggleRepository>();
            var controller = new TogglesController(repository.Object, new ToggleMapper());

            repository
                .Setup(r => r.GetByIdAsync(testId))
                .ReturnsAsync((Toggle) null);

            var result = await controller.Get(testId);

            Assert.IsAssignableFrom<NotFoundResult>(result.Result);
        }

        [Fact]
        public async Task Update_ReturnsNoContent()
        {
            var repository = new Mock<IToggleRepository>();
            var controller = new TogglesController(repository.Object, new ToggleMapper());

            repository
                .Setup(r => r.GetByIdAsync(testId))
                .ReturnsAsync(new Toggle());

            repository
                .Setup(r => r.UpdateAsync(It.IsAny<Toggle>()))
                .ReturnsAsync(true);

            var result = await controller.Update(testId, new ToggleUpdate { Enabled = false });

            Assert.IsAssignableFrom<NoContentResult>(result);
        }

        [Fact]
        public async Task Update_ReturnsNotFound()
        {
            var repository = new Mock<IToggleRepository>();
            var controller = new TogglesController(repository.Object, new ToggleMapper());

            repository
                .Setup(r => r.GetByIdAsync(testId))
                .ReturnsAsync((Toggle) null);

            var result = await controller.Update(testId, new ToggleUpdate { Enabled = false });

            Assert.IsAssignableFrom<NotFoundResult>(result);
        }

        [Fact]
        public async Task Delete_ReturnsNoContent()
        {
            var repository = new Mock<IToggleRepository>();
            var controller = new TogglesController(repository.Object, new ToggleMapper());

            repository
                .Setup(r => r.GetByIdAsync(testId))
                .ReturnsAsync(new Toggle());

            repository
                .Setup(r => r.DeleteAsync(It.IsAny<Toggle>()))
                .ReturnsAsync(true);

            var result = await controller.Delete(testId);

            Assert.IsAssignableFrom<NoContentResult>(result);
        }

        [Fact]
        public async Task Delete_ReturnsNotFound()
        {
            var repository = new Mock<IToggleRepository>();
            var controller = new TogglesController(repository.Object, new ToggleMapper());

            repository
                .Setup(r => r.GetByIdAsync(testId))
                .ReturnsAsync((Toggle) null);

            var result = await controller.Delete(testId);

            Assert.IsAssignableFrom<NotFoundResult>(result);
        }
    }
}
