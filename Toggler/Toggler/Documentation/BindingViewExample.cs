﻿using Swashbuckle.AspNetCore.Filters;
using Toggler.Models.Api;

namespace Toggler.Documentation
{
    public class BindingViewExample : IExamplesProvider<BindingView>
    {
        private readonly IExamplesProvider<BindingCreate> provider;

        public BindingViewExample(IExamplesProvider<BindingCreate> provider)
        {
            this.provider = provider;
        }

        public BindingView GetExamples()
        {
            var create = this.provider.GetExamples();

            return new BindingView
            {
                Id = "60c5980b-de91-4ad0-b698-21d479580a80",
                Toggle = create.Toggle,
                Service = create.Service,
                Enabled = create.Enabled
            };
        }
    }
}
