﻿using Swashbuckle.AspNetCore.Filters;

using Toggler.Models.Api;

namespace Toggler.Documentation
{
    public class ToggleViewExample : IExamplesProvider<ToggleView>
    {
        private readonly IExamplesProvider<ToggleCreate> provider;

        public ToggleViewExample(IExamplesProvider<ToggleCreate> provider)
        {
            this.provider = provider;
        }

        public ToggleView GetExamples()
        {
            var create = this.provider.GetExamples();

            return new ToggleView
            {
                Id = "f8362d4e-7274-4eed-bacc-50b8f3861dad",
                Feature = create.Feature,
                Enabled = create.Enabled
            };
        }
    }
}
