﻿using Swashbuckle.AspNetCore.Filters;

using Toggler.Models.Api;

namespace Toggler.Documentation
{
    public class ToggleCreateExample : IExamplesProvider<ToggleCreate>
    {
        public ToggleCreate GetExamples()
        {
            return new ToggleCreate
            {
                Feature = "IsButtonBlue",
                Enabled = true
            };
        }
    }
}
