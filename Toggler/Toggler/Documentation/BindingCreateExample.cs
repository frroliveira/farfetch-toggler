﻿using Swashbuckle.AspNetCore.Filters;
using Toggler.Models.Api;

namespace Toggler.Documentation
{
    public class BindingCreateExample : IExamplesProvider<BindingCreate>
    {
        private readonly IExamplesProvider<ToggleView> provider;

        public BindingCreateExample(IExamplesProvider<ToggleView> provider)
        {
            this.provider = provider;
        }

        public BindingCreate GetExamples()
        {
            var toggle = this.provider.GetExamples();

            return new BindingCreate
            {
                Toggle = toggle.Id,
                Service = "toggler-ui",
                Enabled = !toggle.Enabled
            };
        }
    }
}
