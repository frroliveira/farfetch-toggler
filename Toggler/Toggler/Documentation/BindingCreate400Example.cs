﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;

namespace Toggler.Documentation
{
    public class BindingCreate400Example : IExamplesProvider<ValidationProblemDetails>
    {
        public ValidationProblemDetails GetExamples()
        {
            return new ValidationProblemDetails
            {
                Title = "One or more validation errors occurred.",
                Status = 400,
                Errors =
                {
                    {
                        "Toggle", new string[] { "The Toggle field is required." }
                    },
                    {
                        "Service", new string[] { "The Service field is required." }
                    }
                }
            };
        }
    }
}
