﻿using Swashbuckle.AspNetCore.Filters;
using Toggler.Models.Api;

namespace Toggler.Documentation
{
    public class BindingUpdateExample : IExamplesProvider<BindingUpdate>
    {
        private readonly IExamplesProvider<BindingView> provider;

        public BindingUpdateExample(IExamplesProvider<BindingView> provider)
        {
            this.provider = provider;
        }

        public BindingUpdate GetExamples()
        {
            var view = this.provider.GetExamples();

            return new BindingUpdate
            {
                Enabled = !view.Enabled
            };
        }
    }
}
