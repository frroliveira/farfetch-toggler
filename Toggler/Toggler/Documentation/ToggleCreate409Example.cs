﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;

namespace Toggler.Documentation
{
    public class ToggleCreate409Example : IExamplesProvider<ProblemDetails>
    {
        public ProblemDetails GetExamples()
        {
            return new ProblemDetails
            {
                Title = "Feature name already in use",
                Status = 409
            };
        }
    }
}