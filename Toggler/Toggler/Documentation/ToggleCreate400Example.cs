﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;

namespace Toggler.Documentation
{
    public class ToggleCreate400Example : IExamplesProvider<ValidationProblemDetails>
    {
        public ValidationProblemDetails GetExamples()
        {
            return new ValidationProblemDetails
            {
                Title = "One or more validation errors occurred.",
                Status = 400,
                Errors = { { "Feature", new string[]{ "The Feature field is required." } } }
            };
        }
    }
}