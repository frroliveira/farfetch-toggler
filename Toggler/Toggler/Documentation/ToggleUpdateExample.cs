﻿using Swashbuckle.AspNetCore.Filters;

using Toggler.Models.Api;

namespace Toggler.Documentation
{
    public class ToggleUpdateExample : IExamplesProvider<ToggleUpdate>
    {
        private readonly IExamplesProvider<ToggleView> provider;

        public ToggleUpdateExample(IExamplesProvider<ToggleView> provider)
        {
            this.provider = provider;
        }

        public ToggleUpdate GetExamples()
        {
            var view = this.provider.GetExamples();

            return new ToggleUpdate
            {
                Enabled = !view.Enabled
            };
        }
    }
}
