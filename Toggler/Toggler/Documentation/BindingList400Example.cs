﻿using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Filters;

namespace Toggler.Documentation
{
    public class BindingList400Example : IExamplesProvider<ValidationProblemDetails>
    {
        public ValidationProblemDetails GetExamples()
        {
            return new ValidationProblemDetails
            {
                Title = "One or more validation errors occurred.",
                Status = 400,
                Errors = { { "toggle", new string[] { "A value for the 'toggle' parameter or property was not provided." } } }
            };
        }
    }
}