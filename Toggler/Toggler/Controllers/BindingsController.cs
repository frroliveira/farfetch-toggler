﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Swashbuckle.AspNetCore.Filters;
using Toggler.Documentation;
using Toggler.Mappers;
using Toggler.Models.Api;
using Toggler.Models.Database;
using Toggler.Repositories;

namespace Toggler.Controllers
{
    [Route("api/bindings")]
    public class BindingsController : ExtendedController
    {
        private readonly IBindingRepository repository;
        private readonly BindingMapper mapper;

        public BindingsController(IBindingRepository repository, BindingMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Creates a new binding
        /// </summary>
        /// <remarks>
        /// The given (toggle, service) pair must be unique. If not specified, binding will be disabled.
        /// </remarks>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(409)]
        [SwaggerResponseExample(400, typeof(BindingCreate400Example))]
        [SwaggerResponseExample(409, typeof(BindingCreate409Example))]
        public async Task<ActionResult<BindingView>> Create([FromBody]BindingCreate request)
        {
            if (await this.repository.GetByServiceAsync(request.Toggle, request.Service) != null)
            {
                return Conflict("Binding with given (toggle, service) pair already exists");
            }

            var binding = this.mapper.ToBinding(request);

            if (await this.repository.CreateAsync(binding))
            {
                var view = this.mapper.ToView(binding);
                return CreatedAtAction(nameof(Get), new { id = binding.Id }, view);
            }

            return InternalServerError("Unable to persist Binding");
        }

        /// <summary>
        /// Retrieves a list of bindings
        /// </summary>
        /// <param name="toggle">Toggle identifier to filter by</param>
        /// <param name="service">Service name to filter by</param>
        [HttpGet]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [SwaggerResponseExample(400, typeof(BindingList400Example))]
        public async Task<ActionResult<IEnumerable<BindingView>>> List(
            [FromQuery, BindRequired]string toggle, [FromQuery]string service)
        {
            List<Binding> bindings;

            if (service != null && service.Length > 0)
            {
                var binding = await this.repository.GetByServiceAsync(toggle, service);
                bindings = binding == null ? new List<Binding>() : new List<Binding> { binding };
            }
            else
            {
                bindings = await this.repository.ListByToggleAsync(toggle);
            }

            var views = bindings.Select(b => this.mapper.ToView(b));
            return Ok(views);
        }

        /// <summary>
        /// Retrieves a specific binding
        /// </summary>
        /// <param name="id">Binding identifier</param>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [SwaggerResponseExample(404, typeof(NotFoundExample))]
        public async Task<ActionResult<BindingView>> Get([FromRoute, BindRequired]string id)
        {
            var binding = await this.repository.GetByIdAsync(id);

            if (binding == null)
            {
                return NotFound();
            }

            return Ok(this.mapper.ToView(binding));
        }

        /// <summary>
        /// Updates a specific binding
        /// </summary>
        /// <param name="id">Binding identifier</param>
        /// <param name="request"></param>
        [HttpPatch("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [SwaggerResponseExample(404, typeof(NotFoundExample))]
        public async Task<ActionResult> Update(string id, [FromBody]BindingUpdate request)
        {
            var binding = await this.repository.GetByIdAsync(id);

            if (binding == null)
            {
                return NotFound();
            }

            binding.Enabled = request.Enabled.Value;

            if (await this.repository.UpdateAsync(binding))
            {
                return NoContent();
            }

            return InternalServerError("Unable to update Binding");
        }

        /// <summary>
        /// Deletes a specific binding
        /// </summary>
        /// <param name="id">Binding identifier</param>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [SwaggerResponseExample(404, typeof(NotFoundExample))]
        public async Task<ActionResult> Delete(string id)
        {
            var binding = await this.repository.GetByIdAsync(id);

            if (binding == null)
            {
                return NotFound();
            }

            if (await this.repository.DeleteAsync(binding))
            {
                return NoContent();
            }

            return InternalServerError("Unable to delete Binding");
        }
    }
}
