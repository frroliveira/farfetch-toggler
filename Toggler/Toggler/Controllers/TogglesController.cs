﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Swashbuckle.AspNetCore.Filters;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Toggler.Documentation;
using Toggler.Mappers;
using Toggler.Models.Api;
using Toggler.Models.Database;
using Toggler.Repositories;

namespace Toggler.Controllers
{
    [Route("api/toggles")]
    public class TogglesController : ExtendedController
    {
        private readonly IToggleRepository repository;
        private readonly ToggleMapper mapper;

        public TogglesController(IToggleRepository repository, ToggleMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Creates a new toggle
        /// </summary>
        /// <remarks>
        /// The given feature must be unique. If not specified, toggle will be disabled.
        /// </remarks>
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(409)]
        [SwaggerResponseExample(400, typeof(ToggleCreate400Example))]
        [SwaggerResponseExample(409, typeof(ToggleCreate409Example))]
        public async Task<ActionResult<ToggleView>> Create([FromBody]ToggleCreate request)
        {
            if (await this.repository.GetByFeatureAsync(request.Feature) != null)
            {
                return Conflict("Feature name already in use");
            }

            var toggle = this.mapper.ToToggle(request);

            if (await this.repository.CreateAsync(toggle))
            {
                var view = this.mapper.ToView(toggle);
                return CreatedAtAction(nameof(Get), new { id = toggle.Id }, view);
            }

            return InternalServerError("Unable to persist Toggle");
        }

        /// <summary>
        /// Retrieves a list of toggles
        /// </summary>
        /// <param name="feature">Feature name to filter by</param>
        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<ActionResult<IEnumerable<ToggleView>>> List([FromQuery]string feature)
        {
            List<Toggle> toggles;

            if (feature != null && feature.Length > 0)
            {
                var toggle = await this.repository.GetByFeatureAsync(feature);
                toggles = toggle == null ? new List<Toggle>() : new List<Toggle> { toggle };
            }
            else
            {
                toggles = await this.repository.ListAsync();
            }

            var views = toggles.Select(t => this.mapper.ToView(t));

            return Ok(views);
        }

        /// <summary>
        /// Retrieves a specific toggle
        /// </summary>
        /// <param name="id">Toggle identifier</param>
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [SwaggerResponseExample(404, typeof(NotFoundExample))]
        public async Task<ActionResult<ToggleView>> Get([FromRoute, BindRequired]string id)
        {
            var toggle = await this.repository.GetByIdAsync(id);

            if (toggle == null)
            {
                return NotFound();
            }

            return Ok(this.mapper.ToView(toggle));
        }

        /// <summary>
        /// Updates a specific toggle
        /// </summary>
        /// <param name="id">Toggle identifier</param>
        /// <param name="request"></param>
        [HttpPatch("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [SwaggerResponseExample(404, typeof(NotFoundExample))]
        public async Task<ActionResult> Update(string id, [FromBody]ToggleUpdate request)
        {
            var toggle = await this.repository.GetByIdAsync(id);

            if (toggle == null)
            {
                return NotFound();
            }

            toggle.Enabled = request.Enabled.Value;

            if (await this.repository.UpdateAsync(toggle))
            {
                return NoContent();
            }

            return InternalServerError("Unable to update Toggle");
        }

        /// <summary>
        /// Deletes a specific toggle
        /// </summary>
        /// <param name="id">Toggle identifier</param>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [SwaggerResponseExample(404, typeof(NotFoundExample))]
        public async Task<ActionResult> Delete(string id)
        {
            var toggle = await this.repository.GetByIdAsync(id);

            if (toggle == null)
            {
                return NotFound();
            }

            if (await this.repository.DeleteAsync(toggle))
            {
                return NoContent();
            }

            return InternalServerError("Unable to delete Toggle");
        }
    }
}