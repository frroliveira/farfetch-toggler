﻿using Microsoft.AspNetCore.Mvc;

namespace Toggler.Controllers
{
    [ApiController]
    [Consumes("application/json")]
    [Produces("application/json")]
    public class ExtendedController : ControllerBase
    {
        protected ObjectResult ProblemResult(int statusCode, string title)
        {
            var details = new ProblemDetails
            {
                Title = title,
                Status = statusCode
            };

            return StatusCode(statusCode, details);
        }

        protected ObjectResult Conflict(string title)
        {
            return ProblemResult(409, title);
        }

        protected ObjectResult InternalServerError(string title)
        {
            return ProblemResult(500, title);
        }
    }
}
