﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Toggler.Models.Database;

namespace Toggler.Repositories
{
    public class ToggleRepository : IToggleRepository
    {
        private readonly TogglerContext context;

        public ToggleRepository(TogglerContext context)
        {
            this.context = context;
        }

        public async Task<Toggle> GetByIdAsync(string id)
        {
            return await this.context.Toggles.FindAsync(id);
        }

        public async Task<Toggle> GetByFeatureAsync(string feature)
        {
            return await this.context.Toggles
                .Where(t => t.Feature == feature)
                .FirstOrDefaultAsync();
        }

        public async Task<List<Toggle>> ListAsync()
        {
            return await this.context.Toggles.ToListAsync();
        }

        public async Task<bool> CreateAsync(Toggle toggle)
        {
            this.context.Toggles.Add(toggle);
            return await this.context.SaveChangesAsync() == 1;
        }

        public async Task<bool> UpdateAsync(Toggle toggle)
        {
            this.context.Toggles.Update(toggle);
            return await this.context.SaveChangesAsync() == 1;
        }

        public async Task<bool> DeleteAsync(Toggle toggle)
        {
            this.context.Toggles.Remove(toggle);
            return await this.context.SaveChangesAsync() == 1;
        }
    }
}
