﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Toggler.Models.Database;

namespace Toggler.Repositories
{
    public class BindingRepository : IBindingRepository
    {
        private readonly TogglerContext context;

        public BindingRepository(TogglerContext context)
        {
            this.context = context;
        }

        public async Task<Binding> GetByIdAsync(string id)
        {
            return await this.context.Bindings.FindAsync(id);
        }

        public async Task<Binding> GetByServiceAsync(string toggle, string service)
        {
            return await this.context.Bindings
                .Where(b => b.Toggle == toggle && b.Service == service)
                .FirstOrDefaultAsync();
        }

        public async Task<List<Binding>> ListByToggleAsync(string toggle)
        {
            return await this.context.Bindings
                .Where(b => b.Toggle == toggle)
                .ToListAsync();
        }

        public async Task<bool> CreateAsync(Binding binding)
        {
            this.context.Bindings.Add(binding);
            return await this.context.SaveChangesAsync() == 1;
        }

        public async Task<bool> UpdateAsync(Binding binding)
        {
            this.context.Bindings.Update(binding);
            return await this.context.SaveChangesAsync() == 1;
        }

        public async Task<bool> DeleteAsync(Binding binding)
        {
            this.context.Bindings.Remove(binding);
            return await this.context.SaveChangesAsync() == 1;
        }
    }
}
