﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Toggler.Models.Database;

namespace Toggler.Repositories
{
    public interface IBindingRepository
    {
        Task<Binding> GetByIdAsync(string id);

        Task<Binding> GetByServiceAsync(string toggle, string service);

        Task<List<Binding>> ListByToggleAsync(string toggle);

        Task<bool> CreateAsync(Binding binding);

        Task<bool> UpdateAsync(Binding binding);

        Task<bool> DeleteAsync(Binding binding);
    }
}
