﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Toggler.Models.Database;

namespace Toggler.Repositories
{
    public interface IToggleRepository
    {
        Task<Toggle> GetByIdAsync(string id);

        Task<Toggle> GetByFeatureAsync(string feature);

        Task<List<Toggle>> ListAsync();

        Task<bool> CreateAsync(Toggle toggle);

        Task<bool> UpdateAsync(Toggle toggle);

        Task<bool> DeleteAsync(Toggle toggle);
    }
}
