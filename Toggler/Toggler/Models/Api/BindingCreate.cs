﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Toggler.Models.Api
{
    public class BindingCreate
    {
        /// <summary>
        /// Toggle identifier
        /// </summary>
        [Required]
        public string Toggle { get; set; }

        /// <summary>
        /// The name of the service
        /// </summary>
        [Required]
        public string Service { get; set; }

        /// <summary>
        /// Whether the binding is enabled
        /// </summary>
        [DefaultValue(false)]
        public bool Enabled { get; set; }
    }
}
