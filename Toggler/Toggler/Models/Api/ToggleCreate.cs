﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Toggler.Models.Api
{
    public class ToggleCreate
    {
        /// <summary>
        /// Unique feature name
        /// </summary>
        [Required]
        public string Feature { get; set; }

        /// <summary>
        /// Whether the toggle is enabled
        /// </summary>
        [DefaultValue(false)]
        public bool Enabled { get; set; }
    }
}
