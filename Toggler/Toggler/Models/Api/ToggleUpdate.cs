﻿using System.ComponentModel.DataAnnotations;

namespace Toggler.Models.Api
{
    public class ToggleUpdate
    {
        /// <summary>
        /// Whether the toggle should be enabled
        /// </summary>
        [Required]
        public bool? Enabled { get; set; }
    }
}
