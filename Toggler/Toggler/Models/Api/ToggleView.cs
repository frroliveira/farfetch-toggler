﻿namespace Toggler.Models.Api
{
    public class ToggleView
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Unique feature name
        /// </summary>
        public string Feature { get; set; }

        /// <summary>
        /// Whether the toggle is enabled
        /// </summary>
        public bool Enabled { get; set; }
    }
}
