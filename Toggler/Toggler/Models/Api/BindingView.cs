﻿namespace Toggler.Models.Api
{
    public class BindingView
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Toggle identifier
        /// </summary>
        public string Toggle { get; set; }

        /// <summary>
        /// The name of the service
        /// </summary>
        public string Service { get; set; }

        /// <summary>
        /// Whether the binding is enabled
        /// </summary>
        public bool Enabled { get; set; }
    }
}
