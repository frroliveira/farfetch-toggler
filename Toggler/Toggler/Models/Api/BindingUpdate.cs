﻿using System.ComponentModel.DataAnnotations;

namespace Toggler.Models.Api
{
    public class BindingUpdate
    {
        /// <summary>
        /// Whether the binding should be enabled
        /// </summary>
        [Required]
        public bool? Enabled { get; set; }
    }
}