﻿namespace Toggler.Models.Database
{
    public class Toggle
    {
        public string Id { get; set; }
        public string Feature { get; set; }
        public bool Enabled { get; set; }
    }
}
