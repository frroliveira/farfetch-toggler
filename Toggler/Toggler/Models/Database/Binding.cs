﻿namespace Toggler.Models.Database
{
    public class Binding
    {
        public string Id { get; set; }
        public string Toggle { get; set; }
        public string Service { get; set; }
        public bool Enabled { get; set; }
    }
}
