﻿using Microsoft.EntityFrameworkCore;

namespace Toggler.Models.Database
{
    public class TogglerContext : DbContext
    {
        public virtual DbSet<Toggle> Toggles { get; set; }
        public virtual DbSet<Binding> Bindings { get; set; }

        public TogglerContext()
        {
        }

        public TogglerContext(DbContextOptions<TogglerContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Toggle>()
                .HasIndex(t => t.Feature)
                .IsUnique();

            modelBuilder
                .Entity<Binding>()
                .HasIndex(b => b.Toggle);

            modelBuilder
                .Entity<Binding>()
                .HasIndex(b => b.Service);
        }
    }
}
