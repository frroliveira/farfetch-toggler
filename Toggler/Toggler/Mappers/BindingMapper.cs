﻿using System;
using Toggler.Models.Api;
using Toggler.Models.Database;

namespace Toggler.Mappers
{
    public class BindingMapper
    {
        public BindingView ToView(Binding binding)
        {
            return new BindingView
            {
                Id = binding.Id,
                Toggle = binding.Toggle,
                Service = binding.Service,
                Enabled = binding.Enabled
            };
        }

        public Binding ToBinding(BindingCreate request)
        {
            return new Binding
            {
                Id = Guid.NewGuid().ToString(),
                Toggle = request.Toggle,
                Service = request.Service,
                Enabled = request.Enabled
            };
        }
    }
}
