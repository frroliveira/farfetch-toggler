﻿using System;
using Toggler.Models.Api;
using Toggler.Models.Database;

namespace Toggler.Mappers
{
    public class ToggleMapper
    {
        public ToggleView ToView(Toggle toggle)
        {
            return new ToggleView
            {
                Id = toggle.Id,
                Feature = toggle.Feature,
                Enabled = toggle.Enabled
            };
        }

        public Toggle ToToggle(ToggleCreate request)
        {
            return new Toggle
            {
                Id = Guid.NewGuid().ToString(),
                Feature = request.Feature,
                Enabled = request.Enabled
            };
        }
    }
}
