﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using Toggler.Mappers;
using Toggler.Models.Api;
using Toggler.Models.Database;
using Toggler.Repositories;

namespace Toggler
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<ToggleMapper>();
            services.AddScoped<IToggleRepository, ToggleRepository>();

            services.AddScoped<BindingMapper>();
            services.AddScoped<IBindingRepository, BindingRepository>();

            services.AddDbContext<TogglerContext>(opt => opt.UseInMemoryDatabase("TogglerDb"));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerExamplesFromAssemblyOf<ToggleCreate>();
            services.AddSwaggerExamplesFromAssemblyOf<ToggleView>();
            services.AddSwaggerExamplesFromAssemblyOf<ToggleUpdate>();

            services.AddSwaggerExamplesFromAssemblyOf<BindingCreate>();
            services.AddSwaggerExamplesFromAssemblyOf<BindingView>();
            services.AddSwaggerExamplesFromAssemblyOf<BindingUpdate>();

            services.AddSwaggerGen(c =>
            {
                var header =
                    "The Toggler API provides two concepts:\n" +
                    "* **Toggle** - a global definition of a feature flag\n" +
                    "* **Binding** - a service specific feature flag\n\n" +
                    "Implementation tries to be agnostic by imposing as little constraints as possible. " +
                    "For example validation of service names, toggle identifiers, or falling back to " +
                    "global feature state are responsibility of client applications.";
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Toggler API", Version = "v1", Description = header });

                c.ExampleFilters();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.DocumentTitle = "Toggler API";
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Toggler API V1");
            });

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
