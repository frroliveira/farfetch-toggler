import React from 'react';
import { Binding } from './Binding';
import { AddButton } from './AddButton';
import { BindingCreate } from './BindingCreate';
import { ListGroup, ListGroupItem } from 'reactstrap';

export class ToggleBindings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: this.props.toggle,
      bindings: [],
      isCreating: false
    };

    this.updateBindings = this.updateBindings.bind(this);

    this.handleAdd = this.handleAdd.bind(this);
    this.handleCreationCancel = this.handleCreationCancel.bind(this);
    this.handleCreationCompleted = this.handleCreationCompleted.bind(this);
  }

  componentDidMount() {
    this.updateBindings();
  }

  updateBindings() {
    fetch('api/bindings/?toggle=' + this.state.toggle, {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      }
    })
    .then((response) => response.json())
    .then((data) => {
      this.setState({ bindings: data });
    });
  }

  handleAdd(event) {
    this.setState({ isCreating: true });
  }

  handleCreationCancel(event) {
    this.setState({ isCreating: false });
  }

  handleCreationCompleted(toggle) {
    this.setState({ isCreating: false });
    this.updateBindings();
  }

  render () {
    let creation;

    if (this.state.isCreating) {
      creation =
        <ListGroupItem>
          <BindingCreate
              toggle={this.state.toggle}
              onCancel={this.handleCreationCancel}
              onCreate={this.handleCreationCompleted}/>
        </ListGroupItem>;
    }

    var bindings = this.state.bindings.length === 0 ?
        <ListGroupItem>
          Click the + button to create
        </ListGroupItem> :
      this.state.bindings.map((binding) =>
        <ListGroupItem key={binding.id}>
          <Binding binding={binding} onDelete={this.updateBindings}/>
        </ListGroupItem>
      );

    return (
      <ListGroup className={this.props.className}>
        <ListGroupItem>
          <span>Bindings</span>
          <AddButton onClick={this.handleAdd}/>
        </ListGroupItem>
        {creation}
        {bindings}
      </ListGroup>
    );
  }
}