﻿import React from 'react';
import { Container, Navbar, NavbarBrand } from 'reactstrap';
import { Link } from 'react-router-dom';

export class NavMenu extends React.Component {
  render () {
    return (
      <header>
        <Navbar className="border-bottom box-shadow mb-3" light>
          <Container>
            <NavbarBrand tag={Link} to="/">TogglerUI</NavbarBrand>
          </Container>
        </Navbar>
      </header>
    );
  }
}
