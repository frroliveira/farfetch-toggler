import React from 'react';
import { Button } from 'reactstrap';

export class IconButton extends React.Component {
  render () {
    return (
      <Button
          className={this.props.className}
          onClick={this.props.onClick}
          disabled={this.props.disabled}
          color={this.props.color}
          size="sm">
        <span className={this.props.icon}></span>
      </Button>
    );
  }
}
