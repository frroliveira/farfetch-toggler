import React from 'react';
import { IconButton } from './IconButton';
import { CustomInput, Form, FormGroup, Input, Label } from 'reactstrap';

export class ToggleCreate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      feature: '',
      enabled: false,
      error: '',
      isCreating: false
    };

    this.handleStateChange = this.handleStateChange.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleNameChange(event) {
    this.setState({ feature: event.target.value, error: '' });
  }

  handleStateChange(event) {
    this.setState({ enabled: !this.state.enabled });
  }

  handleCancel(event) {
    this.props.onCancel(event);
    event.preventDefault();
  }

  handleSubmit(event) {
    this.setState({ isCreating: true });

    fetch('api/toggles/', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        feature: this.state.feature,
        enabled: this.state.enabled
      }),
    })
    .then((response) => {
      this.setState({ isCreating: false });

      if (response.status === 409) {
        throw new Error("Feature name already exists");
      } else if (response.status !== 201) {
        throw new Error("Unable to create toggle");
      }

      return response.json();
    })
    .then((data) => {
      return this.props.onCreate(data);
    })
    .catch((error) => {
      this.setState({ error: error.message });
    });

    event.preventDefault();
  }

  isFeatureValid() {
    return this.state.feature.length > 0;
  }

  hasErrors() {
    return this.state.error.length > 0;
  }

  errorMessage() {
    if (!this.isFeatureValid()) {
      return "Feature name cannot be empty";
    } else if (this.hasErrors()) {
      return this.state.error;
    } else {
      return '';
    }
  }

  render () {
    var message = this.errorMessage();
    let errors;

    if (message.length > 0) {
      errors =
        <Label
            className="text-danger mt-2"
            size="sm">
          {this.errorMessage()}
        </Label>;
    }

    return (
      <div>
        <Form
            onSubmit={this.handleSubmit}
            inline>
          <FormGroup className="mr-2">
            <CustomInput
                type="checkbox"
                className="custom-switch"
                name="enabled"
                id="flagEnabled"
                onChange={this.handleStateChange}
                disabled={this.state.isCreating}/>
          </FormGroup>
          <FormGroup className="mr-2">
            <Input
                invalid={!this.isFeatureValid()}
                type="text"
                name="feature"
                placeholder="Feature name"
                onChange={this.handleNameChange}
                disabled={this.state.isCreating}/>
          </FormGroup>
          <IconButton
              icon="fa fa-times"
              className="mr-2 float-right"
              onClick={this.handleCancel}
              disabled={this.state.isCreating}/>
          <IconButton
              icon="fa fa-check"
              disabled={!this.isFeatureValid() || this.state.isCreating}/>
        </Form>
        {errors}
      </div>
    );
  }
}
