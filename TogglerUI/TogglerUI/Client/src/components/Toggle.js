﻿import React from 'react';
import { DeleteButton } from './DeleteButton';
import { IconButton } from './IconButton';
import { ToggleBindings } from './ToggleBindings';
import { CustomInput } from 'reactstrap';
import './Toggle.css';

export class Toggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: this.props.toggle,
      isUpdating: false,
      isExpanded: false
    };

    this.handleStateChanged = this.handleStateChanged.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleExpand = this.handleExpand.bind(this);
  }

  switchState() {
    var updated = { ...this.state.toggle };
    updated.enabled = !updated.enabled;

    this.setState({ toggle: updated });

    return updated.enabled;
  }

  handleStateChanged() {
    var enabled = this.switchState();
    this.setState({ isUpdating: true });

    fetch('api/toggles/' + this.state.toggle.id, {
      method: 'PATCH',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        enabled: enabled
      }),
    })
    .then((response) => {
      this.setState({ isUpdating: false });

      if (response.status !== 204) {
        this.switchState();
      }
    })
  }

  handleDelete() {
    fetch('api/toggles/' + this.state.toggle.id, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json'
      }
    })
    .then((response) => {
      this.setState({ isDeleting: false });

      if (response.status === 204) {
        this.props.onDelete();
      }
    })
  }

  handleExpand() {
    this.setState({ isExpanded: !this.state.isExpanded });
  }

  render () {
    var expanded = this.state.isExpanded ? "" : "d-none";

    return (
      <div>
        <div>
          <CustomInput
              type="checkbox"
              id={"stateOf" + this.state.toggle.id}
              checked={this.state.toggle.enabled}
              className="custom-switch"
              onChange={this.handleStateChanged}
              disabled={this.state.isUpdating}
              inline/>
          <span>{this.state.toggle.feature}</span>
          <DeleteButton onDelete={this.handleDelete}/>
          <IconButton
              icon="fa fa-link"
              className="float-right mr-2"
              onClick={this.handleExpand}/>
        </div>
        <ToggleBindings
            toggle={this.state.toggle.id}
            className={expanded + " mt-3"}/>
      </div>
    );
  }
}