import React from 'react';
import { IconButton } from './IconButton';

export class AddButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      experimental: this.props.experimental,
      isButtonBlue: false
    };
  }

  componentDidMount() {
    if (this.state.experimental) {
      this.updateFlag();
    }
  }

  updateFlag() {
    fetch('api/toggles/?feature=IsButtonBlue', {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      }
    })
    .then((response) => response.json())
    .then(async (data) => {
      let enabled;

      if (data.status === undefined && data.length > 0) {
        var appEnabled = await this.fetchAppEnabled(data[0].id);
        enabled = appEnabled !== null ? appEnabled : data[0].enabled;
      } else {
        enabled = false;
      }

      this.setState({ isButtonBlue: enabled });
      setTimeout(() => { this.updateFlag(); }, 5000);
    });
  }

  async fetchAppEnabled(toggle) {
    return fetch('api/bindings/?toggle=' + toggle + '&service=toggler-ui', {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      }
    })
    .then((response) => response.json())
    .then((data) => {
      if (data.status === undefined && data.length > 0) {
        return data[0].enabled;
      }
      return null;
    })
    .catch(() => {
      return null;
    });
  }

  render() {
    var plusColor = this.state.isButtonBlue ? "primary" : "secondary";

    return (
      <IconButton
          icon="fa fa-plus"
          color={plusColor}
          className="float-right"
          onClick={this.props.onClick}/>
    );
  }
}