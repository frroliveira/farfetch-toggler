﻿import React from 'react';
import { Toggle } from './Toggle';
import { AddButton } from './AddButton';
import { ListGroup, ListGroupItem } from 'reactstrap';
import { ToggleCreate } from './ToggleCreate';

export class ToggleEditor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggles: [],
      isCreating: false
    };

    this.updateToggles = this.updateToggles.bind(this);

    this.handleAdd = this.handleAdd.bind(this);
    this.handleCreationCancel = this.handleCreationCancel.bind(this);
    this.handleCreationCompleted = this.handleCreationCompleted.bind(this);
  }

  componentDidMount() {
    this.updateToggles();
  }

  updateToggles() {
    fetch('api/toggles/', {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      }
    })
    .then((response) => response.json())
    .then((data) => {
      this.setState({ toggles: data });
    });
  }

  handleAdd(event) {
    this.setState({ isCreating: true });
  }

  handleCreationCancel(event) {
    this.setState({ isCreating: false });
  }

  handleCreationCompleted(toggle) {
    this.setState({ isCreating: false });
    this.updateToggles();
  }

  render () {
    let creation;

    if (this.state.isCreating) {
      creation =
        <ListGroupItem>
          <ToggleCreate
              onCancel={this.handleCreationCancel}
              onCreate={this.handleCreationCompleted}/>
        </ListGroupItem>;
    }

    var toggles = this.state.toggles.length === 0 ?
        <ListGroupItem>
          None available. Click the + button to create
        </ListGroupItem> :
      this.state.toggles.map((toggle) =>
        <ListGroupItem key={toggle.id}>
          <Toggle toggle={toggle} onDelete={this.updateToggles}/>
        </ListGroupItem>
      );

    return (
      <ListGroup>
        <ListGroupItem>
          <span>Toggles</span>
          <AddButton
              experimental="true"
              onClick={this.handleAdd}/>
        </ListGroupItem>
        {creation}
        {toggles}
      </ListGroup>
    );
  }
}
