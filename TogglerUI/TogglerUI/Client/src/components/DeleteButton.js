import React from 'react';
import { IconButton } from './IconButton';

export class DeleteButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isDeleting: false };

    this.handleDeleteStarted = this.handleDeleteStarted.bind(this);
  }

  handleDeleteStarted(event) {
    this.setState({ isDeleting: true });

    setTimeout(() => { this.setState({ isDeleting: false }); }, 2000);
  }

  render() {
    var view = this.state.isDeleting ?
      <IconButton
          icon="fa fa-check"
          className="float-right"
          onClick={this.props.onDelete}/> :
      <IconButton
          icon="fa fa-trash"
          className="float-right"
          onClick={this.handleDeleteStarted}/>;

    return (view);
  }
}