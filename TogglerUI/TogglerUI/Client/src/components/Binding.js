import React from 'react';
import { DeleteButton } from './DeleteButton';
import { CustomInput } from 'reactstrap';

export class Binding extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      binding: this.props.binding,
      isUpdating: false
    };

    this.handleDelete = this.handleDelete.bind(this);
    this.handleStateChanged = this.handleStateChanged.bind(this);
  }

  switchState() {
    var updated = { ...this.state.binding };
    updated.enabled = !updated.enabled;

    this.setState({ binding: updated });

    return updated.enabled;
  }

  handleStateChanged() {
    var enabled = this.switchState();
    this.setState({ isUpdating: true });

    fetch('api/bindings/' + this.state.binding.id, {
      method: 'PATCH',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        enabled: enabled
      }),
    })
    .then((response) => {
      this.setState({ isUpdating: false });

      if (response.status !== 204) {
        this.switchState();
      }
    })
  }

  handleDelete() {
    fetch('api/bindings/' + this.state.binding.id, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json'
      }
    })
    .then((response) => {
      this.setState({ isDeleting: false });

      if (response.status === 204) {
        this.props.onDelete();
      }
    })
  }

  render() {
    return (
      <div>
        <CustomInput
            type="checkbox"
            id={"stateOf" + this.state.binding.id}
            checked={this.state.binding.enabled}
            className="custom-switch"
            onChange={this.handleStateChanged}
            disabled={this.state.isUpdating}
            inline/>
        <span>{this.state.binding.service}</span>
        <DeleteButton onDelete={this.handleDelete}/>
      </div>
    );
  }
}