﻿import React from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { ToggleEditor } from './components/ToggleEditor';

export default class App extends React.Component {
  render () {
    return (
      <Layout>
        <Route exact path='/' component={ToggleEditor} />
      </Layout>
    );
  }
}
