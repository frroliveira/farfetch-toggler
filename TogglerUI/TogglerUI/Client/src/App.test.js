import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router-dom';
import App from './App';

it('renders without crashing', () => {
  fetch
    .once(JSON.stringify([]))
    .once(JSON.stringify([{
      id: "some-id",
      feature: "IsButtonBlue",
      enabled: true
    }]))
    .once(JSON.stringify([{
      id: "some-id",
      toggle: "some-toggle",
      service: "toggler-ui",
      enabled: true
    }]))
    .mockResponse(JSON.stringify([]));

  const div = document.createElement('div');
  ReactDOM.render(
    <MemoryRouter>
      <App />
    </MemoryRouter>, div);
});
