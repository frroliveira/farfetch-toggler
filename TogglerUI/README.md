# Toggler UI

This solution provices a user interface for managing feature flags in the [Toggler API](../Toggler/)

## Prerequisites

- [Node.js 10.16.0](https://nodejs.org/en/)

## Quick Start

To build the solution run

    dotnet publish

Before starting the UI app **ensure** that the Toggler API is running at https://localhost:5001. Then run

    cd TogglerUI
    dotnet run

To run the Node.js tests

    cd Client
    npm test

*Do you want to see a feature toggle in action?*

1. Create a toggle with feature name **IsButtonBlue**
2. Enable the toggle
3. Wait at most 5 seconds and the "+" button will change to blue
4. Click the "link" button
5. Create a binding with service name **toggler-ui**
6. Disabled the binding
7. See it taking precedence over the toggle value

## Development

Implementation is built mainly using:

* [React](https://reactjs.org/)
* [CreateReactApp](https://facebook.github.io/create-react-app/) that takes care of
    * [WebPack](https://webpack.js.org/)
    * [Babel](https://babeljs.io/)
    * [ESLint](https://eslint.org/)
    * Hot reloading

Other libraries are:

* [Bootstrap](https://getbootstrap.com/) for styling
* [Reactstrap](https://reactstrap.github.io/) for Boostrap React components
* [jest-fetch-mock](https://www.npmjs.com/package/jest-fetch-mock) for mock http requests
* [Font awesome](https://fontawesome.com/) for icons

## Roadmap

* Unit tests
* Login page
* Application configuration
* Logging
* Metrics
* Refactor Toogler API code
* Docker container configuration